# translation of kcm_smserver.pot to Esperanto
# Copyright (C) 2000, 2007 Free Software Foundation, Inc.
# Wolfram Diestel <wolfram@steloj.de>, 2000.
# Cindy McKee <cfmckee@gmail.com>, 2007.
# Oliver Kellogg <olivermkellogg@gmail.com>, 2023.
#
# Minuskloj: ĉ ĝ ĵ ĥ ŝ ŭ   Majuskloj: Ĉ Ĝ Ĵ Ĥ Ŝ Ŭ
#
msgid ""
msgstr ""
"Project-Id-Version: kcmsmserver\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-05-25 00:40+0000\n"
"PO-Revision-Date: 2023-04-08 22:44+0100\n"
"Last-Translator: Oliver Kellogg <olivermkellogg@gmail.com>\n"
"Language-Team: Esperanto <kde-i18n-eo@kde.org>\n"
"Language: eo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#. i18n: ectx: label, entry (confirmLogout), group (General)
#: smserversettings.kcfg:9
#, kde-format
msgid "Confirm logout"
msgstr "Konfirmi elsaluton"

#. i18n: ectx: label, entry (shutdownType), group (General)
#: smserversettings.kcfg:13
#, kde-format
msgid "Default leave option"
msgstr "Defaŭlta elir-opcio"

#. i18n: ectx: label, entry (loginMode), group (General)
#: smserversettings.kcfg:22
#, kde-format
msgid "On login"
msgstr "Ĉe ensaluto"

#. i18n: ectx: label, entry (excludeApps), group (General)
#: smserversettings.kcfg:26
#, kde-format
msgid "Applications to be excluded from session"
msgstr "Aplikaĵoj kiujn vi volas ekskluzivi de seanco"

#: ui/main.qml:31
#, kde-format
msgid ""
"The system must be restarted before manual session saving becomes active."
msgstr ""
"La sistemo devas esti reŝargata antaŭ ol mana seanckonservado aktiviĝas."

#: ui/main.qml:36
#, kde-format
msgid "Restart"
msgstr "Reŝargi"

#: ui/main.qml:58
#, kde-format
msgid "Failed to request restart to firmware setup: %1"
msgstr "Malsukcesis peti rekomencon de la agordo de la firmware: %1"

#: ui/main.qml:59
#, kde-format
msgid ""
"Next time the computer is restarted, it will enter the UEFI setup screen."
msgstr ""
"La venontan fojon kiam la komputilo estos rekomencita, ĝi eniros la agordan "
"ekranon de UEFI."

#: ui/main.qml:60
#, kde-format
msgid ""
"Next time the computer is restarted, it will enter the firmware setup screen."
msgstr ""
"La venontan fojon, kiam la komputilo estos rekomencita, ĝi eniros la ekranon "
"de agordo de firmware."

#: ui/main.qml:65
#, kde-format
msgid "Restart Now"
msgstr "Restartigi Nun"

#: ui/main.qml:74
#, kde-format
msgctxt "@title:group"
msgid "Logout Screen"
msgstr "Elsaulta Ekrano"

#: ui/main.qml:78
#, kde-format
msgid "Show:"
msgstr "Montri:"

#: ui/main.qml:92
#, kde-kuit-format
msgctxt "@info"
msgid ""
"When this setting is turned on, the logout confirmation screen will be shown "
"when you log out, shut down, restart, press the power button, or click on "
"buttons or menu items labeled <interface>Leave…</interface>."
msgstr ""
"Kiam ĉi tiu agordo estas ŝaltita, la ekrano de konfirmo de elsalutiĝo estos "
"montrata kiam vi elsalutas, malŝaltas, rekomencas, premas la ŝaltilon aŭ "
"alklakas butonojn aŭ menueroj etikeditaj <interface>Forlasi…</interface>."

#: ui/main.qml:98
#, kde-format
msgctxt "@title:group"
msgid "Session Restore"
msgstr "Restarigo de Seanco"

#: ui/main.qml:106
#, kde-format
msgid "On login, launch apps that were open:"
msgstr "Je ensaluto, lanĉ-aplikaĵoj estis malfermitaj:"

#: ui/main.qml:107
#, kde-format
msgctxt "@option:radio Automatic style of session restoration"
msgid "On last logout"
msgstr "Je lasta elsaluto"

#: ui/main.qml:120
#, kde-format
msgctxt "@option:radio Manual style of session restoration"
msgid "When session was manually saved"
msgstr "Kiam seanco estis mane konservita"

#: ui/main.qml:129
#, kde-kuit-format
msgctxt "@info"
msgid ""
"A <interface>Save Session</interface> button will appear in the "
"<interface>Application Launcher</interface> menu. When you click it, Plasma "
"will remember the apps that are open and restore them on the next login. "
"Click it again to replace the set of remembered apps."
msgstr ""
"Butono <interface>Konservi Seancon</interface> aperos en la menuo "
"<interface>Aplika Lanĉilo</interface>. Kiam vi klakas ĝin, Plasma memoros la "
"apojn malfermitajn kaj restarigos ilin ĉe la sekva ensaluto. Alklaku ĝin "
"denove por anstataŭigi la aron de memoritaj programoj."

#: ui/main.qml:134
#, kde-format
msgctxt ""
"@option:radio Here 'session' refers to the technical concept of session "
"restoration, whereby the windows that were open on logout are re-opened on "
"the next login"
msgid "Start with an empty session"
msgstr "Komenci en malplena seanco"

#: ui/main.qml:148
#, kde-format
msgid "Ignored applications:"
msgstr "Ignorataj aplikaĵoj:"

#: ui/main.qml:171
#, kde-format
msgid ""
"Write apps' executable names here (separated by commas or colons, for "
"example 'xterm:konsole' or 'xterm,konsole') to prevent them from "
"autostarting along with other session-restored apps."
msgstr ""
"Skribu la efektivigeblajn nomojn de aplikaĵoj ĉi tie (apartigite per komoj "
"aŭ dupunktoj, ekzemple 'xterm:konsole' aŭ 'xterm,konsole') por malhelpi ilin "
"aŭtomate ekfunkciigi kune kun aliaj seancaj restaŭrigitaj apps."

#: ui/main.qml:178
#, kde-format
msgctxt "@title:group"
msgid "Firmware"
msgstr "Firmvaro"

#: ui/main.qml:184
#, kde-format
msgctxt ""
"@label:check part of a sentence: After next restart enter UEFI/Firmware "
"setup screen"
msgid "After next restart:"
msgstr "Post sekva restarto:"

#: ui/main.qml:185
#, kde-format
msgctxt "@option:check"
msgid "Enter UEFI setup screen"
msgstr "Eniri UEFI-agordan ekranon"

#: ui/main.qml:186
#, kde-format
msgctxt "@option:check"
msgid "Enter firmware setup screen"
msgstr "Eniri la ekranon de agordo de firmware"
