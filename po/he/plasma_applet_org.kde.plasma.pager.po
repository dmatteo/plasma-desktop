# translation of plasma_applet_pager.po to hebrew
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Diego Iastrubni <elcuco@kde.org>, 2008, 2013.
# tahmar1900 <tahmar1900@gmail.com>, 2008.
# Tahmar1900 <tahmar1900@gmail.com>, 2011.
# elkana bardugo <ttv200@gmail.com>, 2016.
# Elkana Bardugo <ttv200@gmail.com>, 2017. #zanata
# SPDX-FileCopyrightText: 2023 Yaron Shahrabani <sh.yaron@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_org.kde.plasma.pager\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-23 00:38+0000\n"
"PO-Revision-Date: 2023-10-22 09:01+0300\n"
"Last-Translator: Yaron Shahrabani <sh.yaron@gmail.com>\n"
"Language-Team: צוות התרגום של KDE ישראל\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n == 1) ? 0 : ((n == 2) ? 1 : ((n > 10 && "
"n % 10 == 0) ? 2 : 3));\n"
"X-Generator: Lokalize 23.08.1\n"

#: package/contents/config/config.qml:13
#, kde-format
msgid "General"
msgstr "כללי"

#: package/contents/ui/configGeneral.qml:38
#, kde-format
msgid "General:"
msgstr "כללי:"

#: package/contents/ui/configGeneral.qml:40
#, kde-format
msgid "Show application icons on window outlines"
msgstr "הצגת סמלי יישומים במתארי החלונות"

#: package/contents/ui/configGeneral.qml:45
#, kde-format
msgid "Show only current screen"
msgstr "הצגת המסך הנוכחי בלבד"

#: package/contents/ui/configGeneral.qml:50
#, kde-format
msgid "Navigation wraps around"
msgstr "ניווט חוזר להתחלה"

#: package/contents/ui/configGeneral.qml:62
#, kde-format
msgid "Layout:"
msgstr "פריסה:"

#: package/contents/ui/configGeneral.qml:64
#, kde-format
msgctxt "The pager layout"
msgid "Default"
msgstr "ברירת מחדל"

#: package/contents/ui/configGeneral.qml:64
#, kde-format
msgid "Horizontal"
msgstr "אופקית"

#: package/contents/ui/configGeneral.qml:64
#, kde-format
msgid "Vertical"
msgstr "אנכית"

#: package/contents/ui/configGeneral.qml:78
#, kde-format
msgid "Text display:"
msgstr "תצוגת טקסט:"

#: package/contents/ui/configGeneral.qml:81
#, kde-format
msgid "No text"
msgstr "ללא טקסט"

#: package/contents/ui/configGeneral.qml:89
#, kde-format
msgid "Activity number"
msgstr "מספר פעילות"

#: package/contents/ui/configGeneral.qml:89
#, kde-format
msgid "Desktop number"
msgstr "מס׳ שולחן עבודה"

#: package/contents/ui/configGeneral.qml:97
#, kde-format
msgid "Activity name"
msgstr "שם פעילות"

#: package/contents/ui/configGeneral.qml:97
#, kde-format
msgid "Desktop name"
msgstr "שם שולחן העבודה"

#: package/contents/ui/configGeneral.qml:111
#, kde-format
msgid "Selecting current Activity:"
msgstr "בחירת הפעילות הנוכחית:"

#: package/contents/ui/configGeneral.qml:111
#, kde-format
msgid "Selecting current virtual desktop:"
msgstr "בחירת שולחן העבודה הווירטואלי הנוכחי:"

#: package/contents/ui/configGeneral.qml:114
#, kde-format
msgid "Does nothing"
msgstr "לא עושה כלום"

#: package/contents/ui/configGeneral.qml:122
#, kde-format
msgid "Shows the desktop"
msgstr "מציג את שולחן העבודה"

#: package/contents/ui/main.qml:86
#, kde-format
msgid "…and %1 other window"
msgid_plural "…and %1 other windows"
msgstr[0] "…וחלון אחד נוסף"
msgstr[1] "…ושני חלונות נוספים"
msgstr[2] "…ו־%1 חלונות נוספים"
msgstr[3] "…ו־%1 חלונות נוספים"

#: package/contents/ui/main.qml:353
#, kde-format
msgid "%1 Window:"
msgid_plural "%1 Windows:"
msgstr[0] "חלון אחד:"
msgstr[1] "שני חלונות:"
msgstr[2] "%1 חלונות:"
msgstr[3] "%1 חלונות:"

#: package/contents/ui/main.qml:365
#, kde-format
msgid "%1 Minimized Window:"
msgid_plural "%1 Minimized Windows:"
msgstr[0] "חלון ממוזער:"
msgstr[1] "שני חלונות ממוזערים:"
msgstr[2] "%1 חלונות ממוזערים:"
msgstr[3] "%1 חלונות ממוזערים:"

#: package/contents/ui/main.qml:442
#, kde-format
msgid "Desktop %1"
msgstr "שולחן עבודה %1"

#: package/contents/ui/main.qml:443
#, kde-format
msgctxt "@info:tooltip %1 is the name of a virtual desktop or an activity"
msgid "Switch to %1"
msgstr "מעבר אל %1"

#: package/contents/ui/main.qml:588
#, kde-format
msgid "Show Activity Manager…"
msgstr "הצגת מנהל פעילות…"

#: package/contents/ui/main.qml:594
#, kde-format
msgid "Add Virtual Desktop"
msgstr "הוספת שולחן עבודה וירטואלי"

#: package/contents/ui/main.qml:600
#, kde-format
msgid "Remove Virtual Desktop"
msgstr "הסרת שולחן עבודה וירטואלי"

#: package/contents/ui/main.qml:607
#, kde-format
msgid "Configure Virtual Desktops…"
msgstr "הגדרות שולחנות עבודה וירטואליים…"

#~ msgid "Icons"
#~ msgstr "סמלים"
