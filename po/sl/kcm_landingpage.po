# Copyright (C) 2024 This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Matjaž Jeran <matjaz.jeran@amis.net>, 2021, 2022.
# SPDX-FileCopyrightText: 2024 Jure Repinc <jlp@holodeck1.com>
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-06-07 00:40+0000\n"
"PO-Revision-Date: 2024-03-02 00:24+0100\n"
"Last-Translator: Jure Repinc <jlp@holodeck1.com>\n"
"Language-Team: Slovenian <lugos-slo@lugos.si>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 24.04.70\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n"
"%100==4 ? 3 : 0);\n"

#. i18n: ectx: label, entry (colorScheme), group (General)
#: landingpage_kdeglobalssettings.kcfg:9
#, kde-format
msgid "Color scheme name"
msgstr "Ime barvne sheme"

#. i18n: ectx: label, entry (singleClick), group (KDE)
#: landingpage_kdeglobalssettings.kcfg:15
#, kde-format
msgid "Single click to open files"
msgstr "Enojni klik odpira datoteke"

#. i18n: ectx: label, entry (lookAndFeelPackage), group (KDE)
#: landingpage_kdeglobalssettings.kcfg:19
#, kde-format
msgid "Global Look and Feel package"
msgstr "Paket globalnega videza in občutka"

#. i18n: ectx: label, entry (defaultLightLookAndFeel), group (KDE)
#. i18n: ectx: label, entry (defaultDarkLookAndFeel), group (KDE)
#: landingpage_kdeglobalssettings.kcfg:23
#: landingpage_kdeglobalssettings.kcfg:27
#, kde-format
msgid "Global Look and Feel package, alternate"
msgstr "Paket globalnega videza in občutka, alternativni"

#. i18n: ectx: label, entry (animationDurationFactor), group (KDE)
#: landingpage_kdeglobalssettings.kcfg:31
#, kde-format
msgid "Animation speed"
msgstr "Hitrost animacije"

#: ui/main.qml:33
#, kde-format
msgid "Theme:"
msgstr "Tema:"

#: ui/main.qml:77
#, kde-format
msgid "Change Wallpaper…"
msgstr "Spremeni ozadje …"

#: ui/main.qml:84
#, kde-format
msgid "More Appearance Settings…"
msgstr "Več nastavitev izgleda …"

#: ui/main.qml:116
#, kde-format
msgid "Animation speed:"
msgstr "Hitrost animacije:"

#: ui/main.qml:125
#, kde-format
msgctxt "Animation speed"
msgid "Slow"
msgstr "Počasna"

#: ui/main.qml:132
#, kde-format
msgctxt "Animation speed"
msgid "Instant"
msgstr "Hipna"

#: ui/main.qml:146
#, kde-format
msgctxt ""
"part of a sentence: 'Clicking files or folders [opens them/selects them]'"
msgid "Clicking files or folders:"
msgstr "Klikanje na datoteke ali mape:"

#: ui/main.qml:154
#, kde-format
msgctxt "part of a sentence: 'Clicking files or folders selects them'"
msgid "Selects them"
msgstr "Jih izbere"

#: ui/main.qml:159
#, kde-format
msgid "Open by double-clicking instead"
msgstr "Odpri z dvojnim klikom"

#: ui/main.qml:184
#, kde-format
msgctxt "part of a sentence: 'Clicking files or folders opens them'"
msgid "Opens them"
msgstr "Jih odpre"

#: ui/main.qml:189
#, kde-format
msgid "Select by clicking on item's selection marker"
msgstr "Izberi s klikom na označevalnik predmeta"

#: ui/main.qml:214
#, kde-format
msgid "More Behavior Settings…"
msgstr "Več nastavitev obnašanja …"

#: ui/main.qml:226
#, kde-format
msgid "Most Used Pages:"
msgstr "Najpogostejše uporabljane strani:"

#~ msgid "Plasma"
#~ msgstr "Plasma"

#~ msgid ""
#~ "You can help KDE improve Plasma by contributing information on how you "
#~ "use it, so we can focus on things that matter to you.<br/><br/"
#~ ">Contributing this information is optional and entirely anonymous. We "
#~ "never collect your personal data, files you use, websites you visit, or "
#~ "information that could identify you."
#~ msgstr ""
#~ "KDEju lahko pomagate izboljšati Plasmo tako, da prispevate informacije o "
#~ "tem, kako jo uporabljate, zato da se lahko osredotočimo na stvari, ki so "
#~ "za vas pomembne.<br/><br/>Prispevanje teh informacij ni obvezno in je "
#~ "popolnoma anonimno. Nikoli ne zbiramo vaših osebnih podatkov, datotek, ki "
#~ "jih uporabljate, spletnih mest, ki jih obiskujete, ali informacij, ki bi "
#~ "vas lahko identificirale."

#~ msgid "No data will be sent."
#~ msgstr "Nobenih podatkov ne bo poslano."

#~ msgid "The following information will be sent:"
#~ msgstr "Poslane bodo naslednje informacije:"

#~ msgid "Send User Feedback:"
#~ msgstr "Pošlji povratne informacije uporabnika:"

#~ msgctxt "Adjective; as in, 'light theme'"
#~ msgid "Light"
#~ msgstr "Svetla"

#~ msgctxt "Adjective; as in, 'dark theme'"
#~ msgid "Dark"
#~ msgstr "Temna"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Matjaž Jeran"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "matjaz.jeran@amis.net"

#~ msgid "Quick Settings"
#~ msgstr "Hitre nastavitve"

#~ msgid "Landing page with some basic settings."
#~ msgstr "Pristajalna stran z nekaterimi osnovnimi nastavitvami."

#~ msgid "Marco Martin"
#~ msgstr "Marco Martin"

#~ msgid "Most used module number %1"
#~ msgstr "Najpogosteje uporabljan modul številka %1"

#~ msgid "Enable file indexer"
#~ msgstr "Omogoči indeksirnik datotek"

#~ msgid "File Indexing:"
#~ msgstr "Indeksiranje datoteke:"

#~ msgid "Enabled"
#~ msgstr "Omogočeno"
