# Copyright (C) 2024 This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# SPDX-FileCopyrightText: 2021, 2022, 2024 Freek de Kruijf <freekdekruijf@kde.nl>
# SPDX-FileCopyrightText: 2024 pieter <pieterkristensen@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-06-11 00:40+0000\n"
"PO-Revision-Date: 2024-06-11 10:33+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 24.05.0\n"

#: kcmtablet.cpp:32
#, kde-format
msgid "Default"
msgstr "Standaard"

#: kcmtablet.cpp:33
#, kde-format
msgid "Portrait"
msgstr "Portret"

#: kcmtablet.cpp:34
#, kde-format
msgid "Landscape"
msgstr "Landschap"

#: kcmtablet.cpp:35
#, kde-format
msgid "Inverted Portrait"
msgstr "Omgekeerd portret"

#: kcmtablet.cpp:36
#, kde-format
msgid "Inverted Landscape"
msgstr "Omgekeerd landschap"

#: kcmtablet.cpp:86
#, kde-format
msgid "Follow the Current Screen"
msgstr "Het huidige scherm volgen"

#: kcmtablet.cpp:91
#, kde-format
msgid "All Screens"
msgstr "Alle schermen"

#: kcmtablet.cpp:100
#, kde-format
msgctxt "model - (x,y widthxheight)"
msgid "%1 - (%2,%3 %4×%5)"
msgstr "%1 - (%2,%3 %4×%5)"

#: kcmtablet.cpp:160
#, kde-format
msgid "Fit to Screen"
msgstr "Op het scherm laten passen"

#: kcmtablet.cpp:161
#, kde-format
msgid "Keep Aspect Ratio and Fit Within Screen"
msgstr "Beeldverhouding behouden en op het scherm laten passen"

#: kcmtablet.cpp:162
#, kde-format
msgid "Map to Portion of Screen"
msgstr "Op gedeelte van het scherm weergeven"

#: ui/main.qml:29
#, kde-format
msgid "No drawing tablets found"
msgstr "Geen tekentabletten gevonden"

#: ui/main.qml:30
#, kde-format
msgid "Connect a drawing tablet"
msgstr "Met een tekentablet verbinden"

#: ui/main.qml:38
#, kde-format
msgctxt "Tests tablet functionality like the pen"
msgid "Test Tablet…"
msgstr "Tablet testen…"

#: ui/main.qml:58
#, kde-format
msgctxt "@label:listbox The device we are configuring"
msgid "Device:"
msgstr "Apparaat:"

#: ui/main.qml:96
#, kde-format
msgid "Map to screen:"
msgstr "Op scherm weergeven:"

#: ui/main.qml:116
#, kde-format
msgid "Orientation:"
msgstr "Oriëntatie:"

#: ui/main.qml:128
#, kde-format
msgid "Left-handed mode:"
msgstr "Modus linkshandig:"

#: ui/main.qml:139
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Tells the device to accommodate left-handed users. Effects will vary by "
"device, but often it reverses the pad buttonsʼ functionality so the tablet "
"can be used upside-down."
msgstr ""
"Vertelt het apparaat om linkshandige gebruikers tegemoet te komen. Effecten "
"zullen variëren per apparaat, maar vaak draait het de functies van de "
"knoppen op het pad om zodat het tablet ondersteboven gebruikt kan worden."

#: ui/main.qml:145
#, kde-format
msgid "Mapped Area:"
msgstr "Gebied van weergave:"

#: ui/main.qml:232
#, kde-format
msgid "Resize the tablet area"
msgstr "Het tabletgebied van grootte wijzigen"

#: ui/main.qml:256
#, kde-format
msgctxt "@option:check"
msgid "Lock aspect ratio"
msgstr "Beeldverhouding vergrendelen"

#: ui/main.qml:264
#, kde-format
msgctxt "tablet area position - size"
msgid "%1,%2 - %3×%4"
msgstr "%1,%2 - %3×%4"

#: ui/main.qml:273
#, kde-format
msgid "Pen button 1:"
msgstr "Penknop 1:"

#: ui/main.qml:274
#, kde-format
msgid "Pen button 2:"
msgstr "Penknop 2:"

#: ui/main.qml:275
#, kde-format
msgid "Pen button 3:"
msgstr "Penknop 3:"

#: ui/main.qml:319
#, kde-format
msgctxt "@label:listbox The pad we are configuring"
msgid "Pad:"
msgstr "Blok:"

#: ui/main.qml:330
#, kde-format
msgid "None"
msgstr "Geen"

#: ui/main.qml:352
#, kde-format
msgid "Pad button %1:"
msgstr "Padknop %1:"

#: ui/Tester.qml:27
#, kde-format
msgctxt "@title"
msgid "Tablet Tester"
msgstr "Tablettester"

#: ui/Tester.qml:48
#, kde-format
msgid "Stylus press X=%1 Y=%2"
msgstr "Stylus ingedrukt op X=%1 Y=%2"

#: ui/Tester.qml:56
#, kde-format
msgid "Stylus release X=%1 Y=%2"
msgstr "Stylus losgelaten X=%1 Y=%2"

#: ui/Tester.qml:64
#, kde-format
msgid "Stylus move X=%1 Y=%2"
msgstr "Stylus verplaatst naar X=%1 Y=%2"

#: ui/Tester.qml:186
#, kde-format
msgid ""
"## Legend:\n"
"# X, Y - event coordinate\n"
msgstr ""
"## Legenda:\n"
"# X, Y - coördinaten van gebeurtenis\n"

#: ui/Tester.qml:193
#, kde-format
msgctxt "Clear the event log"
msgid "Clear"
msgstr "Wissen"

#~ msgid "Primary (default)"
#~ msgstr "Primary (standaard)"

#~ msgid "Fit to Output"
#~ msgstr "In uitvoer laten passen"

#~ msgid "Fit Output in tablet"
#~ msgstr "Uitvoer in tablet laten passen"

#~ msgid "Custom size"
#~ msgstr "Aangepaste grootte"

#~ msgid "Target display:"
#~ msgstr "Doel-beeldscherm:"

#~ msgid "Area:"
#~ msgstr "Gebied:"

#~ msgid "Tool Button 1"
#~ msgstr "Hulpmiddelknop 1"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Freek de Kruijf - 2021"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "freekdekruijf@kde.nl"
