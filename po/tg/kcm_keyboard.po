# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Victor Ibragimov <victor.ibragimov@gmail.com>, 2008, 2019.
msgid ""
msgstr ""
"Project-Id-Version: kcmkeyboard\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-06-11 00:40+0000\n"
"PO-Revision-Date: 2019-09-16 14:13+0500\n"
"Last-Translator: Victor Ibragimov <victor.ibragimov@gmail.com>\n"
"Language-Team: Tajik <kde-i18n-doc@kde.org>\n"
"Language: tg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 19.04.3\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Victor Ibragimov"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "victor.ibragimov@gmail.com"

#: bindings.cpp:24
#, kde-format
msgid "Keyboard Layout Switcher"
msgstr ""

#: bindings.cpp:26
#, kde-format
msgid "Switch to Next Keyboard Layout"
msgstr ""

#: bindings.cpp:30
#, kde-format
msgid "Switch to Last-Used Keyboard Layout"
msgstr ""

#: bindings.cpp:60
#, kde-format
msgid "Switch keyboard layout to %1"
msgstr ""

#: keyboardmodel.cpp:35
#, kde-format
msgctxt "unknown keyboard model vendor"
msgid "Unknown"
msgstr "Номаълум"

#: keyboardmodel.cpp:39
#, kde-format
msgctxt "vendor | keyboard model"
msgid "%1 | %2"
msgstr ""

#: tastenbrett/main.cpp:52
#, fuzzy, kde-format
#| msgid "Preview"
msgctxt "app display name"
msgid "Keyboard Preview"
msgstr "Пешнамоиш"

#: tastenbrett/main.cpp:54
#, kde-format
msgctxt "app description"
msgid "Keyboard layout visualization"
msgstr ""

#: tastenbrett/main.cpp:139
#, kde-format
msgctxt "@label"
msgid ""
"The keyboard geometry failed to load. This often indicates that the selected "
"model does not support a specific layout or layout variant. This problem "
"will likely also present when you try to use this combination of model, "
"layout and variant.\n"
"Previewing layouts that are defined outside your systems xkb directory is "
"not supported and  will also trigger this message. These might still work "
"fine if applied"
msgstr ""

#: ui/Advanced.qml:40
#, kde-format
msgctxt "@option:checkbox"
msgid "Configure keyboard options"
msgstr ""

#: ui/Hardware.qml:34
#, fuzzy, kde-format
#| msgid "Preview"
msgctxt "@title:group"
msgid "Keyboard model:"
msgstr "Пешнамоиш"

#: ui/Hardware.qml:68
#, kde-format
msgctxt "@title:group"
msgid "NumLock on Plasma Startup:"
msgstr ""

#: ui/Hardware.qml:73
#, fuzzy, kde-format
#| msgid "T&urn on"
msgctxt "@option:radio"
msgid "Turn On"
msgstr "Ф&аъол кардан"

#: ui/Hardware.qml:77
#, fuzzy, kde-format
#| msgid "&Turn off"
msgctxt "@option:radio"
msgid "Turn Off"
msgstr "&Хомӯш кардан"

#: ui/Hardware.qml:81
#, kde-format
msgctxt "@option:radio"
msgid "Leave unchanged"
msgstr ""

#: ui/Hardware.qml:114
#, kde-format
msgctxt "@title:group"
msgid "When key is held:"
msgstr ""

#: ui/Hardware.qml:119
#, kde-format
msgctxt "@option:radio"
msgid "Repeat the key"
msgstr ""

#: ui/Hardware.qml:124
#, kde-format
msgctxt "@option:radio"
msgid "Do nothing"
msgstr ""

#: ui/Hardware.qml:129
#, kde-format
msgctxt "@option:radio"
msgid "Show accented and similar characters"
msgstr ""

#: ui/Hardware.qml:164
#, kde-format
msgctxt "@label:slider"
msgid "Delay:"
msgstr ""

#: ui/Hardware.qml:199
#, kde-format
msgctxt "@label:slider"
msgid "Rate:"
msgstr ""

#: ui/Hardware.qml:247
#, kde-format
msgctxt "@label:textbox"
msgid "Test area:"
msgstr ""

#: ui/Hardware.qml:248
#, kde-format
msgctxt "@info:placeholder"
msgid "Type here to test settings"
msgstr ""

#: ui/LayoutDialog.qml:21
#, kde-format
msgctxt "@title:window"
msgid "Add Layout"
msgstr ""

#: ui/LayoutDialog.qml:106 ui/Layouts.qml:245
#, fuzzy, kde-format
#| msgid "Preview"
msgctxt "@action:button"
msgid "Preview"
msgstr "Пешнамоиш"

#: ui/LayoutDialog.qml:124
#, kde-format
msgctxt "@label:textbox"
msgid "Display text:"
msgstr ""

#: ui/LayoutDialog.qml:143
#, kde-format
msgctxt "@option:textbox"
msgid "Shortcut:"
msgstr ""

#: ui/Layouts.qml:28
#, kde-format
msgctxt "@title:group"
msgid "Shortcuts for Switching Layout"
msgstr ""

#: ui/Layouts.qml:34
#, kde-format
msgctxt "@option:textbox"
msgid "Main shortcuts:"
msgstr ""

#: ui/Layouts.qml:61
#, kde-format
msgctxt "@option:textbox"
msgid "3rd level shortcuts:"
msgstr ""

#: ui/Layouts.qml:88
#, kde-format
msgctxt "@option:textbox"
msgid "Alternative shortcut:"
msgstr ""

#: ui/Layouts.qml:109
#, kde-format
msgctxt "@option:textbox"
msgid "Last used shortcuts:"
msgstr ""

#: ui/Layouts.qml:129
#, kde-format
msgctxt "@option:checkbox"
msgid "Show a popup on layout changes"
msgstr ""

#: ui/Layouts.qml:145
#, kde-format
msgctxt "@title:group"
msgid "Switching Policy"
msgstr ""

#: ui/Layouts.qml:152
#, kde-format
msgctxt "@option:radio"
msgid "Global"
msgstr ""

#: ui/Layouts.qml:156
#, fuzzy, kde-format
#| msgid "&Desktop"
msgctxt "@option:radio"
msgid "Desktop"
msgstr "&Мизи корӣ"

#: ui/Layouts.qml:160
#, fuzzy, kde-format
#| msgid "&Application"
msgctxt "@option:radio"
msgid "Application"
msgstr "&Барнома"

#: ui/Layouts.qml:164
#, kde-format
msgctxt "@option:radio"
msgid "Window"
msgstr ""

#: ui/Layouts.qml:185
#, fuzzy, kde-format
#| msgid "&Enable keyboard repeat"
msgctxt "@option:checkbox"
msgid "Configure Layouts"
msgstr "&Фаъол кардани рафтори тугмаҳои клавиатура"

#: ui/Layouts.qml:200
#, kde-format
msgctxt "@action:button"
msgid "Add"
msgstr ""

#: ui/Layouts.qml:206
#, fuzzy, kde-format
#| msgid "Remove"
msgctxt "@action:button"
msgid "Remove"
msgstr "Тоза кардан"

#: ui/Layouts.qml:213
#, kde-format
msgctxt "@action:button"
msgid "Move Up"
msgstr ""

#: ui/Layouts.qml:229
#, kde-format
msgctxt "@action:button"
msgid "Move Down"
msgstr ""

#: ui/Layouts.qml:316
#, fuzzy, kde-format
#| msgctxt "layout map name"
#| msgid "Map"
msgctxt "@title:column"
msgid "Map"
msgstr "Харита"

#: ui/Layouts.qml:323
#, fuzzy, kde-format
#| msgid "Label"
msgctxt "@title:column"
msgid "Label"
msgstr "Тамға"

#: ui/Layouts.qml:332
#, kde-format
msgctxt "@title:column"
msgid "Layout"
msgstr ""

#: ui/Layouts.qml:337
#, kde-format
msgctxt "@title:column"
msgid "Variant"
msgstr ""

#: ui/Layouts.qml:342
#, kde-format
msgctxt "@title:column"
msgid "Shortcut"
msgstr ""

#: ui/Layouts.qml:388
#, kde-format
msgctxt "@action:button"
msgid "Reassign shortcut"
msgstr ""

#: ui/Layouts.qml:404
#, kde-format
msgctxt "@action:button"
msgid "Cancel assignment"
msgstr ""

#: ui/Layouts.qml:417
#, kde-format
msgctxt "@option:checkbox"
msgid "Spare layouts"
msgstr ""

#: ui/Layouts.qml:453
#, kde-format
msgctxt "@label:spinbox"
msgid "Main layout count:"
msgstr ""

#: ui/main.qml:43
#, kde-format
msgctxt "@title:tab"
msgid "Hardware"
msgstr ""

#: ui/main.qml:49
#, kde-format
msgctxt "@title:tab"
msgid "Layouts"
msgstr ""

#: ui/main.qml:55
#, kde-format
msgctxt "@title:tab"
msgid "Key Bindings"
msgstr ""

#: xkboptionsmodel.cpp:160
#, kde-format
msgid "%1 shortcut"
msgid_plural "%1 shortcuts"
msgstr[0] ""
msgstr[1] ""

#: xkboptionsmodel.cpp:163
#, kde-format
msgctxt "no shortcuts defined"
msgid "None"
msgstr "Ҳеҷ чиз"

#~ msgid "Label:"
#~ msgstr "Тамға:"

#~ msgctxt "no shortcut defined"
#~ msgid "None"
#~ msgstr "Ҳеҷ чиз"

#~ msgctxt "variant"
#~ msgid "Default"
#~ msgstr "Стандартӣ"

#~ msgid "Turn o&ff"
#~ msgstr "Хомӯш кар&дан"

#~ msgid ""
#~ "If supported, this option allows you to hear audible clicks from your "
#~ "computer's speakers when you press the keys on your keyboard. This might "
#~ "be useful if your keyboard does not have mechanical keys, or if the sound "
#~ "that the keys make is very soft.<p>You can change the loudness of the key "
#~ "click feedback by dragging the slider button or by clicking the up/down "
#~ "arrows on the spin box. Setting the volume to 0% turns off the key click."
#~ msgstr ""
#~ "Если поддерживается, этот параметр позволяет слышать щелчки из динамиков "
#~ "компьютера при нажатии клавиш на клавиатуре. это может быть полезным, "
#~ "если на клавиатуре нет механических клавиш, или если звук от нажатия "
#~ "клавиш слишком тихий.<p>Вы можете изменить громкость щелчков, перемещая "
#~ "ползунок или при помощи стрелок на кнопке-счётчике. Установка громкости "
#~ "на 0 отключает щелчки клавиатуры."

#, fuzzy
#~| msgid "Key click &volume:"
#~ msgid "&Key click volume:"
#~ msgstr "Баландии овози &тугмаҳо:"

#~ msgid ""
#~ "If you check this option, pressing and holding down a key emits the same "
#~ "character over and over again. For example, pressing and holding down the "
#~ "Tab key will have the same effect as that of pressing that key several "
#~ "times in succession: Tab characters continue to be emitted until you "
#~ "release the key."
#~ msgstr ""
#~ "Если установлен этот параметр, нажатие и удержание клавиши приводит к "
#~ "тому, что клавиша выдаёт код символа снова и снова (аналогично нажатию "
#~ "клавиши несколько раз)."

#~ msgid "&Enable keyboard repeat"
#~ msgstr "&Фаъол кардани рафтори тугмаҳои клавиатура"
